# Candy. Unified Style Output for python command line


Candy is a unified style way to print gorgeous outputs in command line using python.
    My aim while i code it, was to have a simple yet comfortable tool to handle on screen messages,
    but saving as much keystrokes as i can.

    With Candy there are just a few more key strokes in:

        print("hello")
        
hello

    and
        cprint("red", "hello")
    
hello

    So if you need a complete framework, this is not for you.
    If you are a scripter, o you don't like to be stuck in beauty codes, Candy is for you.

    Install: Just copy this .py in your project folder. no extra dependencies.

    Usage:

    import candy
    from candy import cprint as cprint

    examples:

    cprint("lgreen", "Hello World")
    ^ prints a light green Hello World
    
Hello World

    candy.info("Task done.")
    ^ print a info message
    
[i] - Task done.


    candy.bar("red", 5)
    ^ prints a progress bar coloured in red, set at 25% (for now, 5 to 5 steps)
      No motion, unless you can use it in loops.
    
#■■■■■               # 25%
#■■■■■■              # 30%
#■■■■■■■             # 35%
#■■■■■■■■■■          # 50%
#■■■■■■■■■■■■■■■     # 75%
#■■■■■■■■■■■■■■■■■■■■# 100%
Enjoy, improve and share it as your wish.
Use the wiki to send feedback o suggest improves.


#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.

Thanks for using the Candy module!!!d
