def about():
    import candy
    from candy import cprint as cprint

    print '''
    Candy is a unified style way to print gorgeous outputs in command
    line using python.

    My aim while i code it, was to have a simple yet comfortable tool
    to handle on screen messages,
    but saving as much keystrokes as i can.

    With Candy there are just a few more key strokes in:

        print('hello')
        '''

    print('hello')

    print '''
    and
        cprint('red', 'hello')
    '''
    cprint('red', 'hello')

    print '''
    So if you need a complete framework, this is not for you.
    If you are a scripter, o you don't like to be stuck in beauty codes,
    Candy is for you.

    INSTALL: NOT NEEDED!! Just copy this candy.py in your project folder.
    no extra depedencies are needed.

    Usage:

    import candy
    from candy import cprint as cprint
    from candy import creturn as creturn

    examples:

    cprint('lgreen', 'Hello World')
    ^ prints a light green Hello World
    '''
    cprint('lgreen', 'Hello World')
    print """
    creturn() works like cprint, but returns a value instead a print
    (usefull, for example using raw_input() or exit() )

    Check the candy.py file to check all available colors.

    But there is a even simplier way to display simplier messages,
    using the yummie candies ;) For Example:

    candy.info('Task done.')
    ^ print a info message
    """
    candy.info('Task done.')
    print """

    So siply, so pretty, isn't it'?

    In the same way theres a couple o predefined candy message displays:
        candy.info()
        candy.warning()
        candy.ok()
        candy.fail()
        candy.error()
        candy.check()

    Also there is a creturn function for predefined raw_input questions:
        candy.ask()

        In future releases, there will be unicode candies even sweetier.
    """

    print """

    Another major feature of candy is the candy Bar.

    Candy Bar is an easy way to display GORGEOUS progress bars, fully
    customizable, featuring a full set of unicode symbols.

    If you don't want to mess up with configs or custom layouts, just
    type:'

    candy.bar('red', 25)
    ^ prints a progress bar coloured in red, at 25%.

    """
    candy.bar('dgreen', 5)
    candy.bar('green', 10)
    candy.bar('lgreen', 20)
    candy.bar('lyellow', 40)
    candy.bar('yellow', 80)
    candy.bar('lred', 100)

    print """

    Obviously, there's more about Bar, as it is a whole python class.

    First, let's create a MyBar object, an instance of Bar class:

    MyBar = candy.Bar(128, 1200, 50, "blue", "rounded")

    (Integers must be given, color and style are optional)
    The 3 integers are respectively: value, max value and size of bar.
    candy.Bar does the job for you calculating percentages, so you just need to
    give it REAL integer values.

    Okay, now we have our bar created, let's draw it!

    MyBar.draw()
"""
    MyBar = candy.Bar(728, 1200, 50, "blue", "rounded")
    MyBar.draw()

    print """
    WOW! isn't it sweeeeeeeeet?

    So now, you should want to display an animated bar, just try this code:

    import candy
    import sys
    import time
    i = 1
    maxval = 30
    while i <= maxval:
        i = i + 1
        d = candy.Bar(i,maxval,100, "green")
        d.draw()
        time.sleep(0.25)

    Mind the Caps in candy.Bars to use it as a simple function (candy.bar() )
    or the complete class candy.Bar()

    Check out the candy.py code to find other styles, colors or symbols to use
    at your will


    Also are available Banner and Table methods. Check testables.py code for usage.
    """

    print 'Enjoy, improve and share it at your will.'
    print 'See you in Bitbucket, your feedback will be apreciated.'
    print """

    This software was released in Nov-2013, under the terms of
    GNU GPL license v.3 and Copyleft (by-nc-sa).


    #This program is free software: you can redistribute it and/or modify
    #it under the terms of the GNU General Public License as published by
    #the Free Software Foundation, either version 3 of the License, or
    #(at your option) any later version.

    #This program is distributed in the hope that it will be useful,
    #but WITHOUT ANY WARRANTY; without even the implied warranty of
    #MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    #GNU General Public License for more details.

    #You should have received a copy of the GNU General Public License
    #along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Joaquin Martinez peluzza -a-t- l33t.es
"""
    print candy.random('Thanks for using Candy!!!')
