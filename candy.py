# Candy
# Color for your python terminal
#
# Released under GPL v3. See below for details.


class Color():
    black = '\033[0;30m'
    dred = '\033[0;31m'
    dgreen = '\033[0;32m'
    dyellow = '\033[0;33m'
    dblue = '\033[0;34m'
    dpurple = '\033[0;35m'
    dcyan = '\033[0;36m'
    lgray = '\033[0;37m'
    red = '\033[0;91m'
    green = '\033[0;92m'
    yellow = '\033[0;93m'
    blue = '\033[0;94m'
    purple = '\033[0;95m'
    cyan = '\033[0;96m'
    gray = '\033[0;97m'
    dgray = '\033[1;30m'
    lred = '\033[1;31m'
    lgreen = '\033[1;32m'
    lyellow = '\033[1;33m'  # actually, it is brown
    lblue = '\033[1;34m'
    lpurple = '\033[1;35m'
    lcyan = '\033[1;36m'
    white = '\033[1;37m'
    none = '\033[0m'
    bulk = {
        'black': '\033[0;30m',
        'dred': '\033[0;31m',
        'dgreen': '\033[0;32m',
        'dyellow': '\033[0;33m',
        'dblue': '\033[0;34m',
        'dpurple': '\033[0;35m',
        'dcyan': '\033[0;36m',
        'lgray': '\033[0;37m',
        'red': '\033[0;91m',
        'green': '\033[0;92m',
        'yellow': '\033[0;93m',
        'blue': '\033[0;94m',
        'purple': '\033[0;95m',
        'cyan': '\033[0;96m',
        'gray': '\033[0;97m',
        'dgray': '\033[1;30m',
        'lred': '\033[1;31m',
        'lgreen': '\033[1;32m',
        'lyellow': '\033[1;33m',
        'lblue': '\033[1;34m',
        'lpurple': '\033[1;35m',
        'lcyan': '\033[1;36m',
        'white': '\033[1;37m',
        'none': '',
    }


class Circle():
    empty = u'\u25cb'
    full = u'\u25cf'
    lh = u'\u25d0'
    lhw = u'\u25d1'
    rh = u'\u25d2'
    rhw = u'\u25d3'
    firstquarter = u'\u25d4'
    lastquarter = u'\u25d5'
    _lehalf = u'\u25d6'
    _rihalf = u'\u25d7'
    bullseye = u'\u25ce'
    fisheye = u'\u25c9'


class Rectangle():
    hfull = u'\u25ac'
    hempty = u'\u25ad'
    vfull = u'\u25ae'
    vempty = u'\u25af'
    diamondfull = u'\u25c6'
    diamondempty = u'\u25c7'


class Square():
    full = u'\u25a0'
    empty = u'\u25a1'
    hgrid = u'\u25a4'
    vgrid = u'\u25a5'
    ortogongrid = u'\u25a6'


class Triangle():
    lo_ri = u'\u25e2'
    lo_le = u'\u25e3'
    up_le = u'\u25e4'
    up_ri = u'\u25e5'
    up_full = u'\u25b2'
    up_empty = u'\u25b3'
    ri_full = u'\u25b6'
    ri_empty = u'\u25b7'
    do_full = u'\u25bc'
    dow_empty = u'\u25bd'
    le_full = u'\u25c0'
    le_empty = u'\u25c1'


class Pipe():
    # single
    up_le = u'\u250c'
    up_ri = u'\u2510'
    do_le = u'\u2514'
    do_ri = u'\u2518'
    mid_le = u'\u2524'
    mid_ri = u'\u251c'
    mid_up = u'\u2534'
    mid_do = u'\u252c'
    cross = u'\u253c'
    vert = u'\u2502'
    horiz = u'\u2500'
    # double
    d_up_le = u'\u2554'
    d_up_ri = u'\u2557'
    d_do_le = u'\u255a'
    d_do_ri = u'\u255d'
    d_mid_le = u'\u2563'
    d_mid_ri = u'\u2560'
    d_mid_up = u'\u2569'
    d_mid_do = u'\u2566'
    d_cross = u'\u256c'
    d_vert = u'\u2551'
    d_horiz = u'\u2550'
    # mixed
    m_mid_le = u'\u255f'
    m_mid_ri = u'\u2562'
    m_mid_up = u'\u2567'
    m_mid_do = u'\u2564'


class Banner():

    def __init__(self, string, msgcolor="none", bannercolor="none",
                 style="none", padding=6,):
        self.msgcolor = msgcolor
        self.bannercolor = bannercolor
        self.style = style
        self.initval = 1
        self.padding = padding   # distance from _le border of the screen
        self.string = string     # custom text displayed with the Banner.
        self.pad = len(self.string) + 4

    def set_style(self):
        if self.msgcolor not in Color.bulk:
            self.msgcolor = Color.none
        else:
            self.msgcolor = Color.bulk.get(self.msgcolor)
        if self.bannercolor not in Color.bulk:
            self.bannercolor = Color.none
        else:
            self.bannercolor = Color.bulk.get(self.bannercolor)
        self.string = self.msgcolor + self.string + Color.none
        if self.style is "unicode":
            self.up_le_corner = Triangle.up_le
            self.up_ri_corner = Triangle.up_ri
            self.do_le_corner = Triangle.lo_le
            self.do_ri_corner = Triangle.lo_ri
            self.h_pad = Square.full
            self.v_pad = Square.full
        if self.style is "pipe":
            self.up_le_corner = Pipe.up_le
            self.up_ri_corner = Pipe.up_ri
            self.do_le_corner = Pipe.do_le
            self.do_ri_corner = Pipe.do_ri
            self.h_pad = Pipe.horiz
            self.v_pad = Pipe.vert
        if self.style is "pipes":
            self.up_le_corner = Pipe.d_up_le
            self.up_ri_corner = Pipe.d_up_ri
            self.do_le_corner = Pipe.d_do_le
            self.do_ri_corner = Pipe.d_do_ri
            self.h_pad = Pipe.d_horiz
            self.v_pad = Pipe.d_vert
        elif self.style is "ascii":
            """
            ========================
            ||  DAMN UGLY BANNER  ||
            ========================
            """
            self.pad = self.pad + 2
            self.up_le_corner = "="
            self.up_ri_corner = "="
            self.do_le_corner = "="
            self.do_ri_corner = "="
            self.h_pad = "="
            self.v_pad = "||"
        else:
            self.up_le_corner = "#"
            self.up_ri_corner = "#"
            self.do_le_corner = "#"
            self.do_ri_corner = "#"
            self.h_pad = "#"
            self.v_pad = "#"

    def draw(self):
        import sys
        self.set_style()
        stringBanner = (
            "%s%s%s%s%s\n%s%s%s%s%s%s%s\n%s%s%s%s%s" % (
                self.bannercolor,
                " " * self.padding,
                self.up_le_corner,
                self.h_pad * self.pad,
                self.up_ri_corner,
                " " * self.padding,
                self.v_pad,
                "  ", self.string,
                "  ", self.bannercolor,
                self.v_pad,
                " " * self.padding,
                self.do_le_corner,
                self.h_pad * self.pad,
                self.do_ri_corner,
                Color.none,))
        sys.stdout.write('\r')
        print(stringBanner)
        sys.stdout.flush()


class Table():

    def __init__(self, data, title="", dcolor="none", tcolor="none",
                 style="none", padding=6,):
        self.dcolor = dcolor
        self.tcolor = tcolor
        self.style = style
        self.padding = padding   # distance from _le border of the screen
        self.data = data         # list containing data.
        self.title = title
        self.lentitle = len(self.title)
        if self.dcolor not in Color.bulk:
            self.dcolor = Color.none
        else:
            self.dcolor = Color.bulk.get(self.dcolor)

        if self.tcolor not in Color.bulk:
            self.tcolor = Color.none
        else:
            self.tcolor = Color.bulk.get(self.tcolor)

        self.title = self.dcolor + self.title + Color.none

    def set_style(self):

        if self.style is "pipes":
            # common
            self.v_pad = Pipe.d_vert
            self.h_pad = Pipe.d_horiz
            # corners for header and footer
            self.up_le_corner = Pipe.d_up_le
            self.up_ri_corner = Pipe.d_up_ri
            self.do_le_corner = Pipe.d_do_le
            self.do_ri_corner = Pipe.d_do_ri
            # row separator
            self.up_plug = Pipe.m_mid_do
            self.do_plug = Pipe.m_mid_up
            self.le_sep = Pipe.m_mid_le
            self.ri_sep = Pipe.m_mid_ri
            self.v_sep = Pipe.vert
            self.mid_sep = Pipe.cross
            self.row_pad = Pipe.horiz

        elif self.style is "ascii":
            self.up_le_corner = "~"
            self.up_ri_corner = "~"
            self.do_le_corner = "~"
            self.do_ri_corner = "~"
            self.h_pad = "~"
            self.v_pad = "|"
            # row separator
            self.up_plug = "~"
            self.do_plug = "~"
            self.le_sep = "|"
            self.ri_sep = "|"
            self.mid_sep = "|"
            self.row_pad = "~"

        else:
            # common
            self.v_pad = "#"
            self.h_pad = "#"
            # corners for header and footer
            self.up_le_corner = "#"
            self.up_ri_corner = "#"
            self.do_le_corner = "#"
            self.do_ri_corner = "#"
            # row separator
            self.up_plug = "#"
            self.do_plug = "#"
            self.le_sep = "#"
            self.ri_sep = "#"
            self.mid_sep = "#"
            self.row_pad = "#"

    def draw(self, dat=False, fst=False, sep=False, lst=False, hdr=False):
        import sys
        self.set_style()
        # rows drawing
        # upper margin
        tfirst = "%s%s%s%s" % (
            " " * self.padding + self.tcolor,
            self.up_le_corner,
            (self.up_plug).join(self.h_pad * (len(str(cell)) + 2)
                                for cell in self.data),
            self.up_ri_corner + Color.none)
        # row itself
        rowdata = "%s%s %s %s" % (
            " " * self.padding + self.tcolor,
            self.v_pad + self.dcolor,
            (" " + self.tcolor + self.v_sep + self.dcolor + " ").join(str(data)
                                                                      for data in self.data),
            self.tcolor + self.v_pad + Color.none)
        separator = "%s%s%s%s" % (
            " " * self.padding + self.tcolor,
            self.le_sep,
            (self.mid_sep).join(self.row_pad * (len(str(cell)) + 2)
                                for cell in self.data),
            self.ri_sep + Color.none)
        # end of table
        tlast = "%s%s%s%s" % (
            " " * self.padding + self.tcolor,
            self.do_le_corner,
            (self.do_plug).join(self.h_pad * (len(str(cell)) + 2)
                                for cell in self.data),
            self.do_ri_corner + Color.none)
        # HEADER of table
        # calc how much padding chars are needed to fill border lines
        self.header_pad = (len(tlast) - 19)  # <== 19 because color characters
        # header of table
        hfirst = ("%s%s%s%s%s" % (
            self.tcolor,
            " " * self.padding,
            self.up_le_corner,
            self.h_pad * (self.header_pad),
            self.up_ri_corner
        ))
        htext = "%s%s%s%s%s%s%s%s" % (
            " " * self.padding,
            self.tcolor,
            self.v_pad,
            "  ",
            self.title,
            " " * (self.header_pad - self.lentitle - 2),
            self.tcolor,
            self.v_pad
        )
        hlast = ("%s%s%s%s%s%s" % (
            self.tcolor,
            " " * self.padding,
            self.do_le_corner,
            self.h_pad * (self.header_pad),
            self.do_ri_corner,
            Color.none
        ))
        # printing options:
        if hdr:
            sys.stdout.write(hfirst + "\n" + htext + "\n" + hlast + "\n")
        if fst:
            sys.stdout.write(tfirst + "\n")
        if dat:
            sys.stdout.write(rowdata + "\n")
        if sep:
            sys.stdout.write(separator + "\n")
        if lst:
            sys.stdout.write(tlast + "\n")
        sys.stdout.flush()


class Bar:

    def __init__(self, x, maxval, size, color="none",
                 style="none", padding=6, string=""):
        self.current = x
        self.maxval = maxval
        self.size = size
        self.color = color
        self.style = style
        self.initval = 1
        self.padding = padding   # distance from _le border of the screen
        self.string = string     # custom text displayed with the bar.

    def maths(self):
        if self.size in [10, 20, 25, 50, 100]:
            self.ratio = 100 // self.size
        else:
            error("Bar Size MUST be in [10, 20, 25, 50, 100]")
        self.percent = ((self.current * 100) / self.maxval)
        self.count = (self.percent // self.ratio)
        self._le = (self.size - self.count)

    def set_style(self):
        if self.color not in Color.bulk:
            self.color = Color.none
        else:
            self.color = Color.bulk.get(self.color)
        self.string = self.color + self.string + Color.none
        if self.style is "rounded":
            self.initchar = Circle._lehalf
            self.endchar = Circle._rihalf
            self.progchar = Square.full
            self.h_pad = " "
        elif self.style is "vslots":
            self.initchar = ""
            self.endchar = ""
            self.progchar = Rectangle.vfull
            self.h_pad = Rectangle.vempty
        elif self.style is "hslots":
            self.initchar = ""
            self.endchar = ""
            self.progchar = Rectangle.hfull
            self.h_pad = Rectangle.hempty
        elif self.style is "bullseye":
            self.initchar = ""
            self.endchar = ""
            self.progchar = Circle.bullseye
            self.h_pad = Circle.empty
        elif self.style is "ascii":
            self.initchar = "["
            self.endchar = "]"
            self.progchar = "="
            self.h_pad = " "
        elif self.style is "bgcolor":
            self.initchar = Color.dgray + "|" + Color.none
            self.endchar = Color.dgray + " |"
            self.progchar = self.color + Square.full
            self.h_pad = Color.dgray + Square.full
        elif self.style is "msdos":
            self.initchar = Color.dgray + "|" + Color.none
            self.endchar = Color.dgray + " |"
            self.progchar = Color.yellow + Square.full
            self.h_pad = Color.dgray + Square.full
        else:
            self.initchar = ""
            self.endchar = ""
            self.progchar = Square.full
            self.h_pad = " "

    def draw(self):
        import sys
        self.maths()
        self.set_style()
        stringbar = (
            "%s%s%s%s%s%s%s %s%% %s" % (
                " " * self.padding,
                self.color,
                self.initchar,
                self.progchar * self.count,
                self.h_pad * self._le,
                self.endchar,
                Color.none,
                self.percent,
                self.string))
        sys.stdout.write('\r')
        sys.stdout.write(stringbar)
        sys.stdout.flush()


def ask(string):
        return Color.purple + '[?] - ' + Color.none + string


def cprint(color, string):
    print "%s %s %s" % (Color.bulk.get(color), string, Color.none)


def creturn(color, string):
    return "%s %s %s" % (Color.bulk.get(color), string, Color.none)


def check(string):
        print Color.purple + '[-] - ' + Color.none + string


def debug(string):
        print Color.lcyan + '[>>>DEBUG>>>] - ' + Color.none + string


def error(string):
        print Color.red + '[E] - ' + string + Color.none


def fail(string):
        print Color.red + '[!] - ' + Color.none + string


def info(string):
        print Color.blue + '[i] - ' + Color.none + string


def ok(string):
        print Color.green + '[+] - ' + Color.none + string


def pad(string):
        print Color.dgray + '    - ' + string + Color.none


def warning(string):
    print Color.yellow + '[!] - ' + Color.none + string


def bar(color, percent):
    x = Bar(percent, 100, 25, color)
    x.draw()
    print "\n"


def rainbow(string):
    for color in Color.bulk.values():
        return color + string + Color.none


# given a string, return a random colorized string. Useless yet fun.
def random(string):
    from random import randint
    chars = []
    for char in list(string):
        fg = str(randint(0, 1))
        bg = str(randint(31, 37))
        ansi = '\033[%s;%sm' % (fg, bg)
        chars.append(str(ansi + char + '\033[0m'))
    return ''.join(chars)


# def separator():
    #cprint(color, ())
    #
# MAIN
if __name__ == '__main__':
    import about
    about.about()
